Project Name: postman_api_testing1

Description of Project: 
               In this project,we tested the API's using postman.There is a collection which contains sample request from API.(http://regres.in/).It contains the following request.
1.POST: to create user.
2.PUT/PATCH: to modify or update user.
3.GET:to retrieve user details.
4.DELETE: to delete users.

Types of variable
1.Enviornment varible.2.global variable.3.collection variable 4.local variable.

Data Drieven Technique
          In Data Drieven Technique we generate Random data from http://learning.postman.com/docs/writing-scripts/script-references/variable-list.
1. Read data from excel file: create excel file with .csv.with data and import it when we run the collection.
2. Read data from json file:  create a json file and import it  when we run the collection.

Request chaining:
 Request chaining is the process of defining flow or sequence in which API should run.
 API arrenged one after another.
 we create predefine chain in which API should execute.
 The method used in postman to create Request chaining is postman.setNextrequest("requestname").
 Request chaining can be achieved only when we are running API'S vai collection runner.

Retry of API:
when we are testing an API in testing enviornment,if we trigger the request we dont getthe response in one go ,in that case we use retry of API.

Newmann
Newmann is a tool which is provided by a postman which generates the user friendly report 
it uses the command line tool which is open source.
Execution ofthe postman collection and generate the report using newmann.
newman run is a command used to execute the collection.
here we download reporter,using npm install newman-reporter-html and  npm install newman-reporter-htmlextra.It generates the more interactive report with all information.


